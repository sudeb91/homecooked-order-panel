import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { OrderComponent } from './order/order.component';
import { LocationComponent } from './location/location.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { OrderSuccessComponent } from './order-success/order-success.component';
import { CmsComponent } from './cms/cms.component';
import { SidebarComponent } from './common/sidebar/sidebar.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  { path: '', redirectTo: '/location', pathMatch: 'full' },
  { path:'order', component:OrderComponent },
  { path:'location', component:LocationComponent },
  { path:'checkout', component:CheckoutComponent },
  { path:'order-success', component:OrderSuccessComponent },
  { path:'about-us', component:CmsComponent },
  { path:'services', component:CmsComponent },
  { path:'contact-us', component:CmsComponent },
  { path:'terms', component:CmsComponent },
  { path:'my-account', component:MyAccountComponent },
  { path:'login', component:LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [OrderComponent,HeaderComponent,FooterComponent,SidebarComponent,LocationComponent,CheckoutComponent,OrderSuccessComponent,CmsComponent,MyAccountComponent,LoginComponent]
