import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})


export class RestService {

  constructor(private http: HttpClient) { }

  endpoint = 'https://www.thehomecooked.com:3000/';
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
  

  callPostApi(url,params): Observable<any> {
    return this.http.post(this.endpoint+url,JSON.stringify(params),this.httpOptions).pipe(
      map(this.extractData));
  }

}
