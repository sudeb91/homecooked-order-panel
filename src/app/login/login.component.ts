import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  rest: any;
  message: any;
  msg_class: string;
  model: any;
  loginForm: boolean;
  otpForm: boolean;
  loginSuccess: boolean;
  deliveryOpen: boolean;
  full_address: any;
  authenticationService: any;
  customer_name: any;
  customer_phone: any;

  constructor() { }

  ngOnInit(): void {
  
}
// Login
loginAction(form) {
  console.log(form.value);
  
  this.rest.callPostApi('customer-login',{mobile:form.value.mobile}).subscribe((response) => {
    if(response.status=="true") {
      this.message = response.msg;
      this.msg_class = 'alert-success';
      
      this.model.mobile_verify = form.value.mobile;
      this.loginForm = false;
      this.otpForm = true;
    }
    else {
     this.message = response.msg;
     this.msg_class = 'alert-danger';
     this.loginSuccess = false;
     this.deliveryOpen = false;
   }
  });

  let restoredLocation = JSON.parse(localStorage.getItem('current_location'));
  this.full_address = restoredLocation.full_address;
}
// Login
verifyOtpAction(form) {
  console.log(form.value);
  
  this.rest.callPostApi('customer-verify-otp',{mobile:form.value.mobile_verify,otp:form.value.otp}).subscribe((response) => {
    if(response.status=="true") {
      form.resetForm();
      this.message = response.msg;
      this.msg_class = 'alert-success';
      
      console.log(response);
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      let user = { customer_id: response.customer_id,customer_name: response.customer_name,customer_phone: response.customer_phone,customer_email: response.customer_email };
      this.authenticationService.login(user);

      // get customer details
      let restoredDetails = JSON.parse(localStorage.getItem('currentUser'));
      console.log(restoredDetails);
      this.customer_name = restoredDetails.customer_name;
      this.customer_phone = restoredDetails.customer_phone;

      this.loginSuccess = true;
      this.deliveryOpen = true;

      // click on step 2 element
      document.getElementById('step-2-tab').click();
    }
    else {
      this.message = response.msg;
      this.msg_class = 'alert-danger';
      this.loginSuccess = false;
      this.deliveryOpen = false;
    }
  });

  let restoredLocation = JSON.parse(localStorage.getItem('current_location'));
  this.full_address = restoredLocation.full_address;
}
}
