import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { AuthenticationService } from '../authentication.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  currentUser: any;
  model: any = {};
  loginForm: boolean;
  otpForm: boolean;
  signupForm: boolean;
  loginSuccess: boolean;
  deliveryOpen: boolean;
  paymentOpen: boolean;

  categories: any = [];
  customer_name: string;
  customer_phone: string;
  full_address: string;
  message: string;
  msg_class: string;
  sub_total: number;
  tot_item: number = 0;
  quantity: any = {};
  cartItems: any[] = JSON.parse(localStorage.getItem('cart_products'));

  constructor(public rest: RestService, private router: Router, public authenticationService: AuthenticationService) {          this.authenticationService.currentUser.subscribe(x => this.currentUser = x); 
  }

  ngOnInit() {
    if(this.currentUser) {
      this.loginSuccess = true;
      this.deliveryOpen = true;

      // click on step 2 element
      document.getElementById('step-2-tab').click();

      // get customer details
      let restoredDetails = JSON.parse(localStorage.getItem('currentUser'));
      console.log(restoredDetails);
      this.customer_name = restoredDetails.customer_name;
      this.customer_phone = restoredDetails.customer_phone;
    }
    else {
      this.loginForm = false;
      this.otpForm = false;
      this.signupForm = false;
      this.loginSuccess = false;
      this.deliveryOpen = false;
      this.paymentOpen = false;
    }

    // Get Current Location
    let restoredLocation = JSON.parse(localStorage.getItem('current_location'));
    if (restoredLocation == null || restoredLocation == '') {
      this.router.navigate(['/location']);
    }
    else {
      this.full_address = restoredLocation.full_address;
    }

    this.getCartItems();
  }

  // Get Cart Items
  getCartItems() {
    let restoredProducts = JSON.parse(localStorage.getItem('cart_products'));

    let subTotal = 0;
    for (let i = 0; i < restoredProducts.length; i++) {
      subTotal = subTotal + parseFloat(restoredProducts[i].tot_price);
    }
    this.sub_total = subTotal;
    this.tot_item = restoredProducts.length;

    if (restoredProducts.length == 0) {
      this.router.navigate(['/order']);
    }
    else {
      return restoredProducts;
    }
  }

  // Decrease Quantity
  decreaseQuantity(event, index) {
    let target = event.target || event.srcElement || event.currentTarget;
    let curQty = parseFloat(target.getAttribute('data-val'));
    let qty = curQty - 1;

    if (qty > 0) {
      let restoredProducts = JSON.parse(localStorage.getItem('cart_products'));
      restoredProducts[index].quantity = qty;
      restoredProducts[index].tot_price = parseFloat(restoredProducts[index].price) * qty;
      localStorage.setItem("cart_products", JSON.stringify(restoredProducts));

      this.cartItems = this.getCartItems();
    }
    else {
      if (confirm('Item will be removed from cart. Do you want to continue?')) {
        let restoredProducts = JSON.parse(localStorage.getItem('cart_products'));
        restoredProducts.splice(index, 1);
        localStorage.setItem("cart_products", JSON.stringify(restoredProducts));
        this.cartItems = this.getCartItems();
      }
    }
  }

  // Increase Quantity
  increaseQuantity(event, index) {
    let target = event.target || event.srcElement || event.currentTarget;
    let curQty = parseFloat(target.getAttribute('data-val'));
    let qty = curQty + 1;

    let restoredProducts = JSON.parse(localStorage.getItem('cart_products'));
    restoredProducts[index].quantity = qty;
    restoredProducts[index].tot_price = parseFloat(restoredProducts[index].price) * qty;
    localStorage.setItem("cart_products", JSON.stringify(restoredProducts));

    this.cartItems = this.getCartItems();
  }

  // Login
  loginAction(form) {
    console.log(form.value);
    
    this.rest.callPostApi('customer-login',{mobile:form.value.mobile}).subscribe((response) => {
      if(response.status=="true") {
        this.message = response.msg;
        this.msg_class = 'alert-success';
        
        this.model.mobile_verify = form.value.mobile;
        this.loginForm = false;
        this.otpForm = true;
      }
      else {
        this.message = response.msg;
        this.msg_class = 'alert-danger';
        this.loginSuccess = false;
        this.deliveryOpen = false;
      }
    });

    let restoredLocation = JSON.parse(localStorage.getItem('current_location'));
    this.full_address = restoredLocation.full_address;
  }

  // Login
  verifyOtpAction(form) {
    console.log(form.value);
    
    this.rest.callPostApi('customer-verify-otp',{mobile:form.value.mobile_verify,otp:form.value.otp}).subscribe((response) => {
      if(response.status=="true") {
        form.resetForm();
        this.message = response.msg;
        this.msg_class = 'alert-success';
        
        console.log(response);
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        let user = { customer_id: response.customer_id,customer_name: response.customer_name,customer_phone: response.customer_phone,customer_email: response.customer_email };
        this.authenticationService.login(user);

        // get customer details
        let restoredDetails = JSON.parse(localStorage.getItem('currentUser'));
        console.log(restoredDetails);
        this.customer_name = restoredDetails.customer_name;
        this.customer_phone = restoredDetails.customer_phone;

        this.loginSuccess = true;
        this.deliveryOpen = true;

        // click on step 2 element
        document.getElementById('step-2-tab').click();
      }
      else {
        this.message = response.msg;
        this.msg_class = 'alert-danger';
        this.loginSuccess = false;
        this.deliveryOpen = false;
      }
    });

    let restoredLocation = JSON.parse(localStorage.getItem('current_location'));
    this.full_address = restoredLocation.full_address;
  }

  // Register
  registerAction(form) {
    
    console.log(form.value);
    this.model.mobile_verify = form.value.mobile;
    this.model.mobile = form.value.mobile;

    this.rest.callPostApi('customer-register',{fullname:form.value.name,email:form.value.email,mobile:form.value.mobile}).subscribe((response) => {
      if(response.status=="true") {
        //form.resetForm();
        this.message = response.msg;
        this.msg_class = 'alert-success';
        
        this.signupForm = false;
        this.otpForm = true;
      }
      else {
        this.message = response.msg;
        this.msg_class = 'alert-danger';
        this.loginSuccess = false;
        this.deliveryOpen = false;
      }
    });

    let restoredLocation = JSON.parse(localStorage.getItem('current_location'));
    this.full_address = restoredLocation.full_address;
  }

  // save delivery address
  saveDelivery() {
    this.deliveryOpen = false;
    this.paymentOpen = true;

    // click on step 3 element
    document.getElementById('step-3-tab').click();

    let restoredProducts = JSON.parse(localStorage.getItem('cart_products'));

    let subTotal = 0;
    for (let i = 0; i < restoredProducts.length; i++) {
      subTotal = subTotal + parseFloat(restoredProducts[i].tot_price);
    }
    this.sub_total = subTotal;
  }


  // array unique
  onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
  }
  // Save Order
  saveOrder() {
    let restoredProducts = JSON.parse(localStorage.getItem('cart_products'));

    let subTotal = 0;
    var arrSellerId = [];
    var arrSellerName = [];
    for (let i = 0; i < restoredProducts.length; i++) {
      subTotal = subTotal + parseFloat(restoredProducts[i].tot_price);

      arrSellerId[i] = restoredProducts[i].seller_id;
      arrSellerName[restoredProducts[i].seller_id] =  restoredProducts[i].seller_name;
    }
    this.sub_total = subTotal;

    // get location
    let restoredLocation = JSON.parse(localStorage.getItem('current_location'));

    // get user information
    let restoredDetails = JSON.parse(localStorage.getItem('currentUser'));

    var arrSellerId = arrSellerId.filter(this.onlyUnique);
    console.log(arrSellerId);

    for(let j=0;j<arrSellerId.length;j++) {

      let foodSubTotal = 0;
      var foods = [];
      for (let i = 0; i < restoredProducts.length; i++) {
        foodSubTotal = foodSubTotal + parseFloat(restoredProducts[i].tot_price);
  
        if(arrSellerId[j]==restoredProducts[i].seller_id) {
          foods[i] = {
            actual_price: restoredProducts[i].tot_price,
            count: restoredProducts[i].quantity,
            currency: "INR",
            discount: "",
            discounted_price: restoredProducts[i].tot_price,
            id: restoredProducts[i].id,
            name: restoredProducts[i].name,
            notes: "",
            photo: restoredProducts[i].photo,
            expected_day: restoredProducts[i].expected_day,
            expected_starting_hour: restoredProducts[i].expected_starting_hour,
            expected_closing_hour: restoredProducts[i].expected_closing_hour
          };
        }
      }

      console.log(foods);
      var array = foods;

      var foods = array.filter(function (el) {
        return el != null;
      });

      console.log(foods);

      console.log(arrSellerName[arrSellerId[j]]);

      let jsonPost = {
        address: restoredLocation.full_address,
        currency: "INR",
        delivery: "1",
        delivery_charges: "0",
        discounted_price: "0",
        foods: foods,
        latitude: restoredLocation.latitude,
        longitude: restoredLocation.longitude,
        net_price: this.sub_total,
        order_date: new Date().getTime(),
        order_status: "processing",
        payment_method: "COD",
        profile_photo: "https://www.thehomecooked.com/media/1555010753.jpeg",
        sale_tax: "0",
        seller_id: arrSellerId[j],
        seller_name: arrSellerName[arrSellerId[j]],
        service_address: "Hirapur Thakur Bari হীরাপুর ঠাকুরবাড়ি, Burnpur, Asansol, West Bengal, India",
        user_email: restoredDetails.customer_email,
        user_id: restoredDetails.customer_id,
        user_name: restoredDetails.customer_name,
        phone: restoredDetails.customer_phone
      };

      this.rest.callPostApi('order-generate',jsonPost).subscribe((response) => {
        if(response.status=="true") {
          this.message = response.msg;
          this.msg_class = 'alert-success';
          console.log(response);
        }
        else {
          this.message = response.msg;
          this.msg_class = 'alert-danger';
        }
      });
    }

    this.router.navigate(['/order-success']);
    
  }

}
